﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
	
	public float destroyRadius = 1.0f; 
	public float maxSpeed = 5.0f;
	private BeeSpawner beeSpawner;

	void Start() {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}
	

	void Update() {
		if (Input.GetButton ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}

		// get the input values
		Vector2 direction;
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");

		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);
	}

}
